addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})

const api = {
  async getAllKeys() {
    let j = await db.list()
    return j.keys
  },
  async putOne(obj) {
    await db.put(obj.id, JSON.stringify(obj))
  },
  async getAll() {
    let j = await this.getAllKeys()
    let u = await Promise.all(j.map(item => this.getOne(item.name)))
    return u
  },
  async getOne(id) {
    let obj = await db.get(id)
    return JSON.parse(obj)
  },
}

async function formatLog(data) {
  let str = "";
  data.forEach(function(item) {
    str = str + `[${item.date}] \n`
  })
  return str;
}

async function handlePost(req) {
  let y = await readRequestBody(req)
  let uu = new Date()
  let theID = Date.now()
  let newRecord = {
    id: theID,
    body: y,
    headers: [...req.headers],
    date: uu.toLocaleString(),
  }

  await db.put(theID, JSON.stringify(newRecord))
  return newRecord
}

async function handleRequest(request) {
  let ret = "";
  try {
    if (request.method === 'POST') {
      let i = await handlePost(request)
      ret = { success: 'true', data: i }
      return new Response(JSON.stringify(ret))
    } else {
      let u = await api.getAll()
     let html = await formatLog(u);
      return new Response(html);
    }
  } catch (err) {
    ret = { success: 'false', data: err.toString() }
    return new Response(JSON.stringify(ret))
  }
  
}

async function readRequestBody(request) {
  let theRet = {};
  const { headers } = request
  const contentType = headers.get('content-type');
  if (contentType.includes('application/json')) {
    let y = await request.json()
    theRet = body;
  } 
  else if (contentType.includes('form')) {
    const formData = await request.formData()
    let body = {};
    for (let entry of formData.entries()) {
      body[entry[0]] = entry[1]
    }
    theRet = body
  } else {
    const body = await request.text()
    theRet = body
  }
  return theRet;
}
